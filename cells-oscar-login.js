{
  const {
    html,
  } = Polymer;
  /**
    `<cells-oscar-login>` Description.

    Example:

    ```html
    <cells-oscar-login></cells-oscar-login>
    ```

    ## Styling
    The following custom properties and mixins are available for styling:

    ### Custom Properties
    | Custom Property     | Selector | CSS Property | Value       |
    | ------------------- | -------- | ------------ | ----------- |
    | --cells-fontDefault | :host    | font-family  |  sans-serif |
    ### @apply
    | Mixins    | Selector | Value |
    | --------- | -------- | ----- |
    | --cells-oscar-login | :host    | {} |

    * @customElement
    * @polymer
    * @extends {Polymer.Element}
    * @demo demo/index.html
  */
  class CellsOscarLogin extends Polymer.Element {

    static get is() {
      return 'cells-oscar-login';
    }

    static get properties() {
      return {
        prop1: {
          type: String,
          value: 'login-oscar'
        },
        name: {
          type: String,
          value: ''
        },
        password: {
          type: String,
          vale: ''
        },
        prop4: {
          type: Boolean,
          value: false,
          notify: true
        }

      };
    }
    valida(name, password) {
      if (this.name === 'oscar' && this.password === 'pass') {
        // this.set('prop4', true);
        this.dispatchEvent(new CustomEvent('login-succes', {detail: this.name}));
        alert('bienvenido');

      } else {
        this.dispatchEvent(new CustomEvent('login-error', {detail: this.name}));

        alert('Credenciales invalidas');

      }
    }

    static get template() {
      return html `
      <style include="cells-oscar-login-styles cells-oscar-login-shared-styles"></style>
      <slot></slot>




          <h2>Login</h2>
          <paper-card>
            <div class="card-content">
              <form class="">

                <paper-input label="Usuario" value="{{name::input}}" name="prop2"></paper-input>

                <paper-input label="Password" value="{{password::input}}" type="password" name="prop3"></paper-input>
                <button type="button" name="button" on-click="valida">Ingresar</button>
              </form>

            </div>


          </paper-card>
      `;
    }
  }

  customElements.define(CellsOscarLogin.is, CellsOscarLogin);
}
